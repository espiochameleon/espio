<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainController extends CI_Controller {

	public function index()
	{
		// panggil file view tanpa ekstensi .php
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}
}
