<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function index()
	{
		// panggil file view tanpa ekstensi .php
		$this->load->view('header');
		$this->load->view('auth/login');
		$this->load->view('footer');
	}

	// Login proses
	public function process() {
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);

		$user_logged_in = false;
		if ($data['username'] === 'admin' && $data['password'] === 'admin') {
			$user_logged_in = true;
		}

		if ($user_logged_in === true)
		{
		    redirect('/dosen', 'refresh');

		} else {
			redirect('/signin', 'refresh');
		}
	}
}
