<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosenController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->check_isvalidated();
    }

	public function index()
	{		
		// panggil file view tanpa ekstensi .php
		$this->load->view('header');
		$this->load->view('dosen');
		$this->load->view('footer');
	}
	
	private function check_isvalidated(){
        if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
}
