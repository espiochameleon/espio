<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SigninController extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->check_isvalidated();
    }

	public function index()
	{
		// panggil file view tanpa ekstensi .php
		$this->load->view('header');
		$this->load->view('signin');
		$this->load->view('footer');
	}

	private function check_isvalidated(){
        if($this->session->userdata('validated')){
            redirect('dosen');
        }
    }
	
	// Login proses
	public function process() {

		// validation rules
		$this->form_validation->set_rules('username', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required',
            array('required' => 'Kolom %s wajib diisi!') // custom validation messages
        );

        // check if data valid
        if ($this->form_validation->run() == FALSE)
        {
        	// show login page again if data not valid validation rules
            $this->index();
        }
        else
        {
        	$this->load->model('login_model');
	        // Validate the user can login
	        $result = $this->login_model->validate();

			if(! $result)
			{
			    $this->index();

			} else {
				redirect('/dosen');
			}
        }
	}
}
