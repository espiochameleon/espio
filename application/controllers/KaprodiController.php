<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KaprodiController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dosen_model');
		$this->load->helper('url_helper');
	}
	public function index()
	{
		// panggil file view tanpa ekstensi .php
		$data['join'] = $this->dosen_model->tasks();
		$this->load->view('header');
		$this->load->view('kaprodi',$data);
		$this->load->view('footer');
	}
}
