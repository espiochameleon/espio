<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller {

	public function index()
	{
		// panggil file view tanpa ekstensi .php
		$this->load->view('header');
		$this->load->view('profile');
		$this->load->view('footer');
	}
	public function logout(){
        $this->session->sess_destroy();
        redirect('signin');
    }
}
