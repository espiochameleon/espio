<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Form_insert extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dosen_model');
		$this->load->helper('url_helper');
		$this->load->database();
	}

	public function index() 
	{ 
		$data['dosen'] = $this->dosen_model->get_dosen();	 
		$this->load->view('insert_form', $data);
	}
	
	public function add_data(){
		$valid = $this->form_validation;
		$nik = $this->input->post('nik');
		$instruction = $this->input->post('instruction');
		$date = $this->input->post('date');
		$time = $this->input->post('time');
		$deadline = $this->input->post('deadline');
		$file = $this->input->post('file');
		$ket = $this->input->post('ket');
		$progress = $this->input->post('progress');

		$data = array(
			'nik' => $nik,
			'instruction' =>$instruction,
			'date' => $date,
			'time' => $time,
			'deadline'=> $deadline,
			'file' => $file,
			'ket' => $ket,
			'progress' => $progress
		);  

		$config['upload_path']		= './assets/file/';
		$config['allowed_types']	= 'pdf|doc|docx|xls|xlsx|ppt|pptx|rtf|zip|rar|7zip';
		$config['max_size']			=  100000;
		$config['file_name']		= $file; 

		$this->load->library('upload', $config);

		if(! $this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('insert_form', $error);
		}else{
			
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];
			$data['file'] = $file_name;

			if($this->db->insert('task', $data)){

				$this->load->view('sukses', $data);	
				
			}
		}
		
	}

}

/* End of file Insert.php */
/* Location: ./application/controllers/Insert.php */