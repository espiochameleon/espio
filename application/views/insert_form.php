<!DOCTYPE html>
<html> 
<head> 
	<title>Input Data</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<style type="text/css">
		.right{
			text-align: right;
		}
		th{
			color: white;
			background-color: purple;
		} 
		.wrapper{
			padding-top: 25px;
		}
	</style>
</head>
<body>
<div class="container wrapper">	
<form method="POST" action="<?= base_url('form_insert/add_data')?>" enctype='multipart/form-data'>
	<table class="table table-bordered" >
		<tr>
			<th>Dosen</th>
			<td class="right">:</td>
			<td><select name="nik">
				<option>Pilih Dosen</option>
				<?php foreach ($dosen as $ds  ){
					echo "<option value='".$ds->nik."'>".$ds->name."</option>";}?>
				</select></td></td>
		</tr>
		<tr>
			<th>Instruksi</th>
			<td class="right">:</td>
			<td><input type="text" name="instruction" required=""></td>
		</tr>
		<tr>
			<th rowspan="2">Waktu</th>
			<td class="right">Tanggal :</td>
			<td><input type="date" name="date" required=""></td>
		</tr>
		<tr>
			<td class="right">Jam :</td>
			<td><input type="time" name="time" required="">WIB</td>
		</tr>
		<tr>
			<th>Deadline</th>
			<td class="right">: </td>
			<td><input type="text" name="deadline" required=""></td>
		</tr>
		<tr>
			<th>Progress</th>
			<td class="right">:</td>
			<td><input type="text" name="progress" required=""></td>
		</tr>
		<tr>
			<th rowspan="3">Keterangan</th>
			<td class="right">add file :</td>
			<td><input type="file" name="file"></td>
		</tr>
		<tr>
			<td class="right">Note :</td>
			<td><input type="text" name="ket"></td>

		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="SEND"></td>
		</tr>
	</table>
</form>
</div>
</body>
</html>

