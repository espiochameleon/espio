<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>Dashboard</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    
  </head>
  <body>  
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>KAPRODI</h1>
		  <div class="square"></div>
        </header>
        <div class="profile-photo-container">
		
          <img src="assets/bootstrap-3.3.7-dist/images/kaprodi.png" alt="Profile Photo" class="img-responsive">  
          <div class="profile-photo-overlayer"></div>
        </div>      
        
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
        </div>
        <nav class="templatemo-left-nav">          
          <ul>
            <li><a href="#" class="active"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-bar-chart fa-fw"></i>BELUM</a></li>
            <li><a href="#"><i class="fa fa-users fa-fw"></i>BELUM</a></li>
            <li><a href="login.html"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>  
        </nav>
      </div>
      <!-- Main content --> 
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a href="" class="active">BELUM</a></li>
                <li><a href="">BELUM</a></li>
                <li><a href="">BELUM</a></li>
                <li><a href="">BELUM</a></li>
              </ul>  
            </nav> 
          </div>
        </div>
<div class="templatemo-content-container">
<div class="templatemo-flex-row flex-content-row">
<div class="container wrapper">

 <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">	
<i class="fa fa-times"></i>
<div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">PENUGASAN</h2></div>
<form method="POST" action="">
	<table class="table table-bordered" >
		<tr>
			<th>Dosen</th>
			<td class="right">:</td>
			<td><select name="">
					<option>Ahlihi Masruro</option>
					<option>Barka Satya</option>
					<option>Firman Asharudin</option>
					<option>Hastari</option>
					<option>Nila Feby P</option>
				</select></td>
		</tr>
		<tr>
			<th>Instruksi</th>
			<td class="right">:</td>
			<td><input type="text" name=""></td>
		</tr>
		<tr>
			<th rowspan="2">Waktu</th>
			<td class="right">Tanggal :</td>
			<td><input type="date" name=""></td>
		</tr>
		<tr>
			<td class="right">Jam :</td>
			<td><input type="time" name="">WIB</td>
		</tr>
		<tr>
			<th>Deadline</th>
			<td class="right">: </td>
			<td><input type="text" name=""></td>
		</tr>
		<tr>
			<th rowspan="2">Keterangan</th>
			<td class="right">add file :</td>
			<td><input type="file" name=""></td>
		</tr>
		<tr>
			<td></td>
			<td><button type="submit" class="templatemo-blue-button">Kirim</button></td>
		</tr>
	</table>
</form>
</div>
          <div class="templatemo-flex-row flex-content-row">   
            <div class="col-1">
              <div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">
                <i class="fa fa-times"></i>
                <div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">Table Penugasan</h2></div>
                <div class="table-responsive">
                  <table class="table table-bordered" >
					<thead>
						<tr>
							<th rowspan="2">NO</th>
							<th rowspan="2">INSTRUKSI</th>
							<th rowspan="2">DOSEN</th>
							<th colspan="3">WAKTU PENUGASAN</th>
							<th rowspan="2">KETERANGAN</th>
							<th rowspan="2">PROGRESS</th>
						</tr>
						<tr>
							<th>Tanggal</th>
							<th>Jam</th>
							<th>Deadline</th>
						</tr>
					</thead>
					<tbody>
					<?php print_r($join);?>
						<?php $no=1; foreach ($join as $task){?>
						<tr>
							<td><?php echo $no++; ?> </td>
							<td><?php echo $task->instruction; ?></td>
							<td><?php echo $task->name; ?></td>
							<td><?php echo $task->date; ?></td>
							<td><?php echo $task->time; ?></td>
							<td><?php echo $task->deadline; ?></td>
							<td><?php echo $task->ket; ?></td>
							<td><?php echo $task->progress; ?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
                </div>                          
              </div>
            </div>           
          </div> <!-- Second row ends -->
          
          <footer class="text-right">
            <p>Copyright &copy; 2018 D3 Teknik Informatika 
            | Redesigned by <a href="http://d3ti.amikom.ac.id" target="_parent">Prodi D3TI Amikom</a></p>
          </footer>         
        </div>
      </div>
    </div>
    
    <!-- JS -->
    <script src="js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
    <script src="js/jquery-migrate-1.2.1.min.js"></script> <!--  jQuery Migrate Plugin -->
    <script src="https://www.google.com/jsapi"></script> <!-- Google Chart -->
    <script>
      /* Google Chart 
      -------------------------------------------------------------------*/
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart); 
      
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Topping');
          data.addColumn('number', 'Slices');
          data.addRows([
            ['Mushrooms', 3],
            ['Onions', 1],
            ['Olives', 1],
            ['Zucchini', 1],
            ['Pepperoni', 2]
          ]);

          // Set chart options
          var options = {'title':'How Much Pizza I Ate Last Night'};

          // Instantiate and draw our chart, passing in some options.
          var pieChart = new google.visualization.PieChart(document.getElementById('pie_chart_div'));
          pieChart.draw(data, options);

          var barChart = new google.visualization.BarChart(document.getElementById('bar_chart_div'));
          barChart.draw(data, options);
      }

      $(document).ready(function(){
        if($.browser.mozilla) {
          //refresh page on browser resize
          // http://www.sitepoint.com/jquery-refresh-page-browser-resize/
          $(window).bind('resize', function(e)
          {
            if (window.RT) clearTimeout(window.RT);
            window.RT = setTimeout(function()
            {
              this.location.reload(false); /* false to get page from cache */
            }, 200);
          });      
        } else {
          $(window).resize(function(){
            drawChart();
          });  
        }   
      });
      
    </script>
    <script type="text/javascript" src="js/templatemo-script.js"></script>      <!-- Templatemo Script -->

  </body>
</html>