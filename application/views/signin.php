<div class="templatemo-content-widget templatemo-login-widget white-bg">
	<header class="text-center">
      <div class="square"></div>
      <h1>Halaman Login</h1>
	  <div class="square"></div>
    </header>
    <!-- <form action="index.html" class="templatemo-login-form"> -->
    <?php echo form_open('signincontroller/process'); ?>
    	<div class="form-group">
    		<span class="text-danger"><?php echo form_error('username'); ?></span>
    		<div class="input-group">
        		<div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>	        		
              	<input name="username" id="name" type="text" class="form-control" placeholder="email@amikom.ac.id">           
          	</div>	
    	</div>
    	<div class="form-group">
    		<span class="text-danger"><?php echo form_error('password'); ?></span>
    		<div class="input-group">
        		<div class="input-group-addon"><i class="fa fa-key fa-fw"></i></div>	        		
              	<input name="password" id="password" type="password" class="form-control" placeholder="******">           
          	</div>	
    	</div>	          	
		<div class="form-group">
			<input type="submit" name="submit" id="submit" value="Login" class="templatemo-blue-button width-100">
			<!--button type="submit" class="templatemo-blue-button width-100">Login</button-->
		</div>
    </form>
</div>
<div class="templatemo-content-widget templatemo-login-widget templatemo-register-widget white-bg">
	<p>Not a registered user yet? <strong><a href="#" class="blue-text">Sign up now!</a></strong></p>
</div>





