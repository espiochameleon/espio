<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>Dashboard</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    
  </head>
  <body>  
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        <header class="templatemo-site-header">
          <div class="square"></div>
          <h1>DOSEN</h1>
		  <div class="square"></div>
        </header>
        <div class="profile-photo-container">

          <img src="assets/bootstrap-3.3.7-dist/images/1.png" alt="Profile Photo" class="img-responsive">  
          <div class="profile-photo-overlayer"></div>
        </div>      
        
        <div class="mobile-menu-icon">
            <i class="fa fa-bars"></i>
        </div>
        <nav class="templatemo-left-nav">          
          <ul>
            <li><a href="#" class="active"><i class="fa fa-home fa-fw"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-bar-chart fa-fw"></i>BELUM</a></li>
            <li><a href="#"><i class="fa fa-users fa-fw"></i>BELUM</a></li>
            <li><a id="logout" href="<?php echo base_url('profilecontroller/logout'); ?>"><i class="fa fa-eject fa-fw"></i>Sign Out</a></li>
          </ul>  
        </nav>
      </div>
      <!-- Main content --> 
      <div class="templatemo-content col-1 light-gray-bg">
        <div class="templatemo-top-nav-container">
          <div class="row">
            <nav class="templatemo-top-nav col-lg-12 col-md-12">
              <ul class="text-uppercase">
                <li><a href="" class="active">BELUM</a></li>
                <li><a href="">BELUM</a></li>
                <li><a href="">BELUM</a></li>
                <li><a href="">BELUM</a></li>
              </ul>  
            </nav> 
          </div>
        </div>
<div class="templatemo-content-container">
<div class="templatemo-flex-row flex-content-row">
<div class="container wrapper">
<div class="panel panel-default templatemo-content-widget white-bg no-padding templatemo-overflow-hidden">	
<i class="fa fa-times"></i>
<div class="panel-heading templatemo-position-relative"><h2 class="text-uppercase">INFORMASI</h2></div>

<form method="POST" >
	<table class="table table-bordered">
		<thead>
			<tr>
				<th colspan="2" >DOSEN</th>
				<th rowspan="3">NO</th>
				<th rowspan="3">INSTRUKSI</th>
				<th colspan="3" rowspan="2">WAKTU PENUGASAN</th>
				<th rowspan="3">KETERANGAN</th>
				<th rowspan="3" colspan="2">PROGRESS</th>
			</tr>
			
			<tr>
				<td>Nama</td>
				<td>$namanya</td>					
			</tr>
			<tr>
				<td>NIK</td>
				<td>$nik</td>
				<th>Tanggal</th>
				<th>Jam</th>
				<th>Deadline</th> 
			</tr>
		</thead>
		
	</table>
</form>
</div>
          <footer class="text-right">
            <p>Copyright &copy; 2018 D3 Teknik Informatika 
            | Redesigned by <a href="http://d3ti.amikom.ac.id" target="_parent">Prodi D3TI Amikom</a></p>
          </footer>         
        </div>
      </div>
  </body>
</html>