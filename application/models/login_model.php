<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Login_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function validate(){
        // grab user input
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        
        // Prep the query
        $this->db->where('user', $username);
        $this->db->where('pass', $password);
        
        // Run the query
        $query = $this->db->get('dosen');
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {

            // If there is a user, then create session data
            $row = $query->row();
            $data = array(
                    'user_id' => $row->id_user,
                    'user' => $row->user,
                    'pass' => $row->pass,
                    'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }
        // If the previous process did not validate
        // then return false.
        die($query->num_rows);
        return false;
    }
}
?>