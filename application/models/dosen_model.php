<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function get_dosen() {
		return $this->db->get('dosen')->result();
	}	
	// public function do_insert($data){
	// 	$this->db->insert('task',$data);
	// }
	public function tasks(){
		$this->db->select('*');
		$this->db->from('task');
		$this->db->join('dosen', 'dosen.nik = task.nik');
		$this->db->order_by('dosen.name','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_dosen_task($nik){
		$nik = $this->session->userdata['logged_in']['nik'];
		$this->db->select("*");
		$this->db->where('dosen',$nik);
		$this->db->from('dosen');
		$this->db->join('task','task.nik = dosen.nik');
		$query = $this->db->get();
		return $query->result();
	}
	//update
	public function update($data,$nik){
		$this->db->where('nik',$nik);
		return $this->db->update('task',$data);
	}	
} 

/* End of file dosen_model.php */
/* Location: ./application/models/dosen_model.php */