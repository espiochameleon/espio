<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}	
	//display all tasks from nik
	public function tasks(){
		$this->db->select("*");
		$this->db->from('task');
		$this->db->join('dosen', 'dosen.nik = task.nik');
		$query = $this->db->get();
		return $query->result();
	}
}

/* End of file task_model.php */
/* Location: ./application/models/task_model.php */